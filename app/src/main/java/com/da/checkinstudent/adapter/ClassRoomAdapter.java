package com.da.checkinstudent.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.da.checkinstudent.R;
import com.da.checkinstudent.model.Student;

import java.util.ArrayList;
import java.util.Set;

public class ClassRoomAdapter extends RecyclerView.Adapter<ClassRoomAdapter.ClassRoomHolder> {

    private LayoutInflater inflater;
    private String[] data;
    private ClassRoomItemListener listener;

    public ClassRoomAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setData(String[] data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(ClassRoomItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ClassRoomHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ClassRoomHolder(inflater.inflate(R.layout.item_class, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ClassRoomHolder holder, int position) {
        holder.bindData(data[position]);
        if (listener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemRoomClicked(data[position]);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.length;
    }

    public class ClassRoomHolder extends RecyclerView.ViewHolder {

        private TextView tvName;

        public ClassRoomHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_class_name);
        }

        private void bindData(String name) {
            tvName.setText(name);
        }
    }

    public interface ClassRoomItemListener {
        void onItemRoomClicked(String name);
    }
}
