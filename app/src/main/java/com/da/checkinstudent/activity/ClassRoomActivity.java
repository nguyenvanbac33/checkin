package com.da.checkinstudent.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.da.checkinstudent.R;
import com.da.checkinstudent.adapter.ClassRoomAdapter;
import com.da.checkinstudent.model.Student;
import com.da.checkinstudent.utils.Constance;
import com.da.checkinstudent.utils.DialogUtils;
import com.da.checkinstudent.views.ActionbarView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ClassRoomActivity extends AppCompatActivity implements ValueEventListener, ClassRoomAdapter.ClassRoomItemListener {
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constance.STUDENT);
    private Set<String> classes = new HashSet<>();
    private RecyclerView lvClasses;
    private ClassRoomAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_room);
        DialogUtils.showProgressDialog(this);
        reference.addValueEventListener(this);
        initViews();
    }

    private void initViews() {
        lvClasses = findViewById(R.id.lv_class);
        adapter = new ClassRoomAdapter(getLayoutInflater());
        adapter.setListener(this);
        lvClasses.setAdapter(adapter);
        ((ActionbarView) findViewById(R.id.actionbar)).setTitle("Class Room");
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot snapshot) {
        ArrayList<Student> arr = new ArrayList<>();
        for (DataSnapshot sn : snapshot.getChildren()) {
            Student st = sn.getValue(Student.class);
            arr.add(st);
            classes.add(st.getRoom());
        }
        String[] c = new String[classes.size()];
        classes.toArray(c);
        adapter.setData(c);
        DialogUtils.dismissProgressDialog();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError error) {

    }

    @Override
    public void onItemRoomClicked(String name) {
        Intent intent;
        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean(Constance.EXTRA_FROM_STUDENT_LIST, false)) {
            intent = new Intent(this, StudentListActivity.class);
        } else {
            intent = new Intent(this, SeatPlanActivity.class);
        }
        intent.putExtra(Constance.EXTRA_CLASS_NAME, name);
        startActivity(intent);
    }
}
