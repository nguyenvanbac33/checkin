package com.da.checkinstudent.utils;

public interface Constance {
    String STUDENT = "student";
    String TEACHER = "teacher";
    String EXTRA_CLASS_NAME = "extra.CLASS.NAME";
    String EXTRA_SEAT = "extra.SEAT";
    String EXTRA_FROM_STUDENT_LIST = "extra.FROM.STUDENT.LIST";
}
