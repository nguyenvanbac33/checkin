package com.da.checkinstudent.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.da.checkinstudent.R;
import com.da.checkinstudent.model.Student;
import com.da.checkinstudent.utils.Constance;
import com.da.checkinstudent.utils.DialogUtils;
import com.da.checkinstudent.views.ActionbarView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class FindInfoActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtStudentId;
    private EditText edtId;
    private AppCompatButton btnFind;
    private Query reference ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_info);
        initViews();
    }

    private void initViews() {
        edtStudentId = findViewById(R.id.edt_student_id);
        edtId = findViewById(R.id.edt_id);
        btnFind = findViewById(R.id.btn_find);
        btnFind.setOnClickListener(this);
        ((ActionbarView) findViewById(R.id.actionbar)).setTitle("Find Student");
    }

    @Override
    public void onClick(View view) {
        String id = edtId.getText().toString();
        String studentId = edtStudentId.getText().toString();
        if (id.isEmpty() || studentId.isEmpty()) {
            Toast.makeText(this, "Id and Student id cannot empty", Toast.LENGTH_SHORT).show();
            return;
        }
        DialogUtils.showProgressDialog(this);
        reference = FirebaseDatabase.getInstance().getReference(Constance.STUDENT).orderByChild("studentId").equalTo(studentId);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                DialogUtils.dismissProgressDialog();
                try {
                    for (DataSnapshot sn : snapshot.getChildren()) {
                        Student s = sn.getValue(Student.class);
                        if (s.getId().equalsIgnoreCase(id)) {
                            reference.removeEventListener(this);
                            FirebaseDatabase.getInstance().getReference(Constance.STUDENT).child(studentId).child("checked").setValue(true);
                            if (TextUtils.isEmpty(s.getAvatar())) {
                                Intent intent = new Intent(FindInfoActivity.this, AvatarActivity.class);
                                intent.putExtra(Student.class.getName(), s);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(FindInfoActivity.this, SeatPlanActivity.class);
                                intent.putExtra(Constance.EXTRA_SEAT, s.getSeat());
                                intent.putExtra(Constance.EXTRA_CLASS_NAME, s.getRoom());
                                startActivity(intent);
                            }
                            return;
                        }
                    }
                    Toast.makeText(FindInfoActivity.this, "Information not found", Toast.LENGTH_SHORT).show();
                } catch (Exception ex) {
                    Toast.makeText(FindInfoActivity.this, "Information not found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
