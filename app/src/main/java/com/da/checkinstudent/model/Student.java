package com.da.checkinstudent.model;

import java.io.Serializable;

public class Student implements Serializable {
    private String id;
    private String studentId;
    private String name;
    private String className;
    private int seat;
    private String shift;
    private String room;
    private String avatar;
    private boolean checked = false;

    public Student() {
    }

    public Student(String id, String studentId, String name, String className, int seat, String shift, String room) {
        this.id = id;
        this.studentId = studentId;
        this.name = name;
        this.className = className;
        this.seat = seat;
        this.shift = shift;
        this.room = room;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
