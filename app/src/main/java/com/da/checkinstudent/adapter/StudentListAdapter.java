package com.da.checkinstudent.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.da.checkinstudent.R;
import com.da.checkinstudent.model.Student;

import java.util.ArrayList;

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.StudentHolder> {

    private LayoutInflater inflater;
    private ArrayList<Student> data;

    public StudentListAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setData(ArrayList<Student> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public ArrayList<Student> getData() {
        return data;
    }

    @NonNull
    @Override
    public StudentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StudentHolder(inflater.inflate(R.layout.item_student_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StudentHolder holder, int position) {
        holder.bindData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class StudentHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvId;
        private TextView tvClass;
        private TextView tvSeat;
        private ImageView imAvatar;

        public StudentHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvId = itemView.findViewById(R.id.tv_id);
            tvClass = itemView.findViewById(R.id.tv_class);
            tvSeat = itemView.findViewById(R.id.tv_seat);
            imAvatar = itemView.findViewById(R.id.im_avatar);
        }

        private void bindData(Student student) {
            tvName.setText("Họ tên: " + student.getName());
            tvId.setText("Mã SV: " + student.getStudentId());
            tvClass.setText("Lớp: " + student.getClassName());
            tvSeat.setText("STT: " + student.getSeat());
            Glide.with(imAvatar).load(student.getAvatar())
                    .circleCrop()
                    .error(R.drawable.ic_student)
                    .into(imAvatar);
        }
    }
}
