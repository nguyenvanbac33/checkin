package com.da.checkinstudent.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.da.checkinstudent.R;
import com.da.checkinstudent.model.Teacher;
import com.da.checkinstudent.utils.Constance;
import com.da.checkinstudent.utils.DialogUtils;
import com.da.checkinstudent.views.ActionbarView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class TeacherLoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtUserName;
    private EditText edtPassword;
    private Button btnLogin;
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constance.TEACHER);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_login);
        initViews();
    }

    private void initViews() {
        edtPassword = findViewById(R.id.edt_password);
        edtUserName = findViewById(R.id.edt_user_name);
        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);
        ((ActionbarView) findViewById(R.id.actionbar)).setTitle("Login");
    }

    @Override
    public void onClick(View view) {
        String userName = edtUserName.getText().toString();
        String password = edtPassword.getText().toString();
        if (userName.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "User name and password cannot blank", Toast.LENGTH_SHORT).show();
            return;
        }
        DialogUtils.showProgressDialog(this);
        reference.orderByChild("username").equalTo(userName).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                DialogUtils.dismissProgressDialog();
                try {
                    for (DataSnapshot sn : snapshot.getChildren()) {
                        Teacher t = sn.getValue(Teacher.class);
                        if (t.getPassword().equalsIgnoreCase(password)) {
                            Intent intent = new Intent(TeacherLoginActivity.this, ClassRoomActivity.class);
                            startActivity(intent);
                            finish();
                            return;
                        }
                    }
                    Toast.makeText(TeacherLoginActivity.this, "Login fail", Toast.LENGTH_SHORT).show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Toast.makeText(TeacherLoginActivity.this, "Login fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
