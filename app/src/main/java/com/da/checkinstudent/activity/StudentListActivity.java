package com.da.checkinstudent.activity;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.da.checkinstudent.R;
import com.da.checkinstudent.adapter.StudentListAdapter;
import com.da.checkinstudent.model.Student;
import com.da.checkinstudent.utils.Constance;
import com.da.checkinstudent.views.ActionbarView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class StudentListActivity extends AppCompatActivity implements ValueEventListener {

    private RecyclerView lvStudent;
    private StudentListAdapter adapter;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);
        String className = getIntent().getStringExtra(Constance.EXTRA_CLASS_NAME);
        ((ActionbarView) findViewById(R.id.actionbar)).setTitle(className);
        database.getReference(Constance.STUDENT).orderByChild("room").equalTo(className).addValueEventListener(this);
        initViews();
    }

    private void initViews() {
        lvStudent = findViewById(R.id.lv_student);
        adapter = new StudentListAdapter(getLayoutInflater());
        lvStudent.setAdapter(adapter);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot snapshot) {
        ArrayList<Student> arr = new ArrayList<>();
        for (DataSnapshot sn : snapshot.getChildren()) {
            Student st = sn.getValue(Student.class);
            arr.add(st);
        }
        adapter.setData(arr);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError error) {

    }
}
