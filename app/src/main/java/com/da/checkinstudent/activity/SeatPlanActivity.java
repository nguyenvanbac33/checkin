package com.da.checkinstudent.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.da.checkinstudent.R;
import com.da.checkinstudent.adapter.StudentAdapter;
import com.da.checkinstudent.dialog.StudentInfoDialog;
import com.da.checkinstudent.model.Student;
import com.da.checkinstudent.utils.Constance;
import com.da.checkinstudent.views.ActionbarView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SeatPlanActivity extends AppCompatActivity implements ValueEventListener, StudentAdapter.StudentItemListener, View.OnClickListener {

    private RecyclerView lvStudent;
    private StudentAdapter adapter;
    private AppCompatButton btnPrint;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private int seat = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seat_plan);
        String className = getIntent().getStringExtra(Constance.EXTRA_CLASS_NAME);
        ((ActionbarView) findViewById(R.id.actionbar)).setTitle(className);
        database.getReference(Constance.STUDENT).orderByChild("room").equalTo(className).addValueEventListener(this);
        seat = getIntent().getExtras().getInt(Constance.EXTRA_SEAT, -1);
        initViews();
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int result = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            return result == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    private void initViews() {
        lvStudent = findViewById(R.id.lv_student);
        adapter = new StudentAdapter(getLayoutInflater(), seat, seat == -1);
        adapter.setListener(this);
        lvStudent.setAdapter(adapter);
        btnPrint = findViewById(R.id.btn_print);
        btnPrint.setOnClickListener(this);
        if (seat == -1) btnPrint.setVisibility(View.VISIBLE); else btnPrint.setVisibility(View.GONE);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot snapshot) {
        ArrayList<Student> arr = new ArrayList<>();
        for (DataSnapshot sn : snapshot.getChildren()) {
            Student st = sn.getValue(Student.class);
            arr.add(st);
        }
        adapter.setData(arr);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError error) {

    }

    @Override
    public void onStudentClicked(Student student) {
        StudentInfoDialog dialog = new StudentInfoDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Student.class.getName(), student);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), null);
    }

    @Override
    public void onClick(View view) {
        if (!checkPermission()) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            return;
        }
        doPrint();
    }

    private void doPrint() {
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        SimpleDateFormat dp = new SimpleDateFormat("YYYY-MM-DD-hh-mm-ss");
        path = path + "/" + dp.format(new Date()) + ".csv";
        File f = new File(path);
        String data = "ID, STUDENT ID, NAME, CLASS NAME, ROOM, SEAT\n";
        for (Student s : adapter.getData()) {
            if (s.isChecked()) continue;
            data += s.getId() + "," + s.getStudentId() + "," + s.getName() + "," + s.getClassName() + "," + s.getRoom() + "," + s.getSeat() + "\n";
        }
        try {
            f.createNewFile();
            FileOutputStream stream = new FileOutputStream(f);
            stream.write(data.getBytes());
            stream.close();
            Toast.makeText(this, "Print success " + path, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Print fail", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (checkPermission()) {
            doPrint();
        }
    }
}
