package com.da.checkinstudent.dialog;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.da.checkinstudent.R;
import com.da.checkinstudent.model.Student;

public class StudentInfoDialog extends DialogFragment implements View.OnClickListener {

    private TextView tvName;
    private TextView tvClassName;
    private TextView tvStudentId;
    private TextView tvId;
    private AppCompatButton btnOk;
    private ImageView imAvatar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_info, container, false);
        tvName = view.findViewById(R.id.tv_name);
        tvClassName = view.findViewById(R.id.tv_class_name);
        tvStudentId = view.findViewById(R.id.tv_student_id);
        tvId = view.findViewById(R.id.tv_id);
        btnOk = view.findViewById(R.id.btn_close);
        imAvatar = view.findViewById(R.id.im_avatar);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnOk.setOnClickListener(this);
        Student student = (Student) getArguments().getSerializable(Student.class.getName());
        tvName.setText(student.getName());
        tvClassName.setText(student.getClassName());
        tvStudentId.setText(student.getStudentId());
        tvId.setText(student.getId());
        Glide.with(imAvatar).load(student.getAvatar())
                .circleCrop()
                .error(R.drawable.ic_student)
                .into(imAvatar);
    }

    @Override
    public void onClick(View view) {
        dismiss();
    }
}
