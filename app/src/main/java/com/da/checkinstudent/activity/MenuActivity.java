package com.da.checkinstudent.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.da.checkinstudent.R;
import com.da.checkinstudent.utils.Constance;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatButton btnStudent;
    private AppCompatButton btnStudentList;
    private AppCompatButton btnTeacher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        initViews();
    }

    private void initViews() {
        btnStudent = findViewById(R.id.btn_student);
        btnTeacher = findViewById(R.id.btn_teacher);
        btnStudentList = findViewById(R.id.btn_student_list);
        btnStudent.setOnClickListener(this);
        btnTeacher.setOnClickListener(this);
        btnStudentList.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btn_student:
                intent = new Intent(this, FindInfoActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btn_student_list:
                intent = new Intent(this, ClassRoomActivity.class);
                intent.putExtra(Constance.EXTRA_FROM_STUDENT_LIST, true);
                startActivity(intent);
                finish();
                break;
            case R.id.btn_teacher:
                intent = new Intent(this, TeacherLoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
