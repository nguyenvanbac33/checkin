package com.da.checkinstudent;

import android.app.Application;

import com.da.checkinstudent.model.Student;
import com.da.checkinstudent.utils.Constance;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class App extends Application {

    private ArrayList<Student> arr = new ArrayList<>();

    {
        arr.add(new Student("1", "17141101", "Nguyễn Văn A", "A1", 1, "Sáng", "301"));
        arr.add(new Student("2", "17141102", "Nguyễn Văn B", "A2", 2, "Sáng", "301"));
        arr.add(new Student("3", "17141103", "Nguyễn Văn C", "A3", 3, "Sáng", "301"));
        arr.add(new Student("4", "17141104", "Nguyễn Văn D", "A4", 4, "Sáng", "301"));
        arr.add(new Student("5", "17141105", "Nguyễn Văn E", "A5", 5, "Sáng", "301"));
        arr.add(new Student("6", "17141106", "Nguyễn Văn F", "A6", 6, "Sáng", "301"));
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        for (Student s: arr ) {
//            FirebaseDatabase.getInstance().getReference(Constance.STUDENT).child(s.getStudentId()).setValue(s);
//        }
    }
}
