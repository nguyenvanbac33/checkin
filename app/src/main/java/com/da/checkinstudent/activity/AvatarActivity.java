package com.da.checkinstudent.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.canhub.cropper.CropImage;
import com.canhub.cropper.CropImageOptions;
import com.canhub.cropper.CropImageView;
import com.da.checkinstudent.R;
import com.da.checkinstudent.model.Student;
import com.da.checkinstudent.utils.Constance;
import com.da.checkinstudent.utils.DialogUtils;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

public class AvatarActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView imAvatar;
    private Button btnSave;
    private Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        student = (Student) getIntent().getSerializableExtra(Student.class.getName());
        setContentView(R.layout.activity_avatar);
        imAvatar = findViewById(R.id.im_avatar);
        btnSave = findViewById(R.id.btn_save);
        imAvatar.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.im_avatar:
                startCrop();
                break;
            case R.id.btn_save:
                if (uriContent == null) {
                    Toast.makeText(this, "Chưa chọn ảnh", Toast.LENGTH_SHORT).show();
                    return;
                }
                DialogUtils.showProgressDialog(this);
                FirebaseStorage.getInstance().getReference().child(student.getId()+".png")
                        .putFile(uriContent)
                        .addOnFailureListener(e -> {
                            Toast.makeText(this, "Save fail", Toast.LENGTH_SHORT).show();
                            DialogUtils.dismissProgressDialog();
                        })
                        .addOnSuccessListener(info -> {
                            info.getStorage().getDownloadUrl().addOnCompleteListener(it -> {
                                if (it.isSuccessful()) {
                                    FirebaseDatabase.getInstance().getReference(Constance.STUDENT).child(student.getStudentId()).child("avatar").setValue(it.getResult().toString());
                                    Intent intent = new Intent(AvatarActivity.this, SeatPlanActivity.class);
                                    intent.putExtra(Constance.EXTRA_SEAT, student.getSeat());
                                    intent.putExtra(Constance.EXTRA_CLASS_NAME, student.getRoom());
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(this, "Save fail", Toast.LENGTH_SHORT).show();
                                }
                                DialogUtils.dismissProgressDialog();
                            });
                        });
                break;
        }
    }

    private Uri uriContent;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                uriContent = result.getUri();
                Glide.with(imAvatar).load(uriContent).into(imAvatar);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void startCrop() {
        CropImage.activity(null).setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setFixAspectRatio(true)
                .setAspectRatio(1, 1)
                .setScaleType(CropImageView.ScaleType.CENTER)
                .start(this);
    }
}
