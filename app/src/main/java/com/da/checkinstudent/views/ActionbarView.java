package com.da.checkinstudent.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.da.checkinstudent.R;

public class ActionbarView extends FrameLayout {

    private TextView tvTitle;

    public ActionbarView(@NonNull Context context) {
        super(context);
        init();
    }

    public ActionbarView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ActionbarView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ActionbarView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.actionbar_view, this, true);
        tvTitle = findViewById(R.id.tv_title);
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }
}
