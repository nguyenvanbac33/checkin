package com.da.checkinstudent.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.da.checkinstudent.R;
import com.da.checkinstudent.model.Student;

import java.util.ArrayList;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentHolder> {

    private LayoutInflater inflater;
    private ArrayList<Student> data;
    private int seat;
    private boolean fromTeacher;
    private StudentItemListener listener;

    public StudentAdapter(LayoutInflater inflater, int seat, boolean fromTeacher) {
        this.seat = seat;
        this.inflater = inflater;
        this.fromTeacher = fromTeacher;
    }

    public void setListener(StudentItemListener listener) {
        this.listener = listener;
    }

    public void setData(ArrayList<Student> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public ArrayList<Student> getData() {
        return data;
    }

    @NonNull
    @Override
    public StudentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StudentHolder(inflater.inflate(R.layout.item_student, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StudentHolder holder, int position) {
        holder.bindData(data.get(position), position);
        if (listener != null) {
            holder.itemView.setOnClickListener(view -> {
                listener.onStudentClicked(data.get(position));
            });
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class StudentHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private ImageView imStudent;
        private View left;
        private View right;

        public StudentHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            imStudent = itemView.findViewById(R.id.im_student);
            left = itemView.findViewById(R.id.padding_left);
            right = itemView.findViewById(R.id.padding_right);
        }

        private void bindData(Student student, int position) {
            tvName.setText(student.getName());
            if (student.getSeat() == seat || (fromTeacher && student.isChecked())) {
                imStudent.setImageResource(R.drawable.ic_student_selected);
            } else {
                imStudent.setImageResource(R.drawable.ic_student);
            }
            left.setVisibility(View.VISIBLE);
            right.setVisibility(View.VISIBLE);
            position = position % 4;
            position = position + 1;
            if (position % 4 == 0) {
                left.setVisibility(View.GONE);
            } else if (position % 3 == 0) {
                right.setVisibility(View.GONE);
            } else if (position % 2 == 0) {
                left.setVisibility(View.GONE);
            } else {
                right.setVisibility(View.GONE);
            }
        }
    }

    public interface StudentItemListener {
        void onStudentClicked(Student student);
    }
}
